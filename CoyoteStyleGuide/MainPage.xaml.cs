﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Telerik.UI.Xaml.Controls.Grid;
using Telerik.UI.Xaml.Controls.Grid.Primitives;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CoyoteStyleGuide
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public object LastItemSelected { get; set; }

        public MainPage()
        {
            this.InitializeComponent();

            this.DataGrid.ItemsSource = new List<Data>
            {
                new Data { Country = "India", Capital = "New Delhi"},
                new Data { Country = "South Africa", Capital = "Cape Town"},
                new Data { Country = "Nigeria", Capital = "Abuja" },
                new Data { Country = "Singapore", Capital = "Singapore" }
            };
        }

        private void DataGrid_OnSelectionChanged(object sender, DataGridSelectionChangedEventArgs e)
        {
            this.DataGrid.ShowRowDetailsForItem(DataGrid.SelectedItem);
        }

        private void DataGrid_OnPointerMoved(object sender, PointerRoutedEventArgs e)
        {
            RadDataGrid g = sender as RadDataGrid;
            
        }
    }

    public class Data
    {
        public string Country { get; set; }

        public string Capital { get; set; }
    }
}
