﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace CoyoteStyleGuide.Themes
{
    public sealed class LoadToolTip : Control
    {
        public static DependencyProperty LoadDisplayTextProperty = DependencyProperty.Register(nameof(LoadDisplayText), typeof(string), typeof(LoadToolTip), new PropertyMetadata(null));
        public static DependencyProperty Row1ToolTipTextProperty = DependencyProperty.Register(nameof(Row1ToolTipText), typeof(string), typeof(LoadToolTip), new PropertyMetadata(null));
        public static DependencyProperty Row2ToolTipTextProperty = DependencyProperty.Register(nameof(Row2ToolTipText), typeof(string), typeof(LoadToolTip), new PropertyMetadata(null));
        public LoadToolTip()
        {
            this.DefaultStyleKey = typeof(LoadToolTip);
        }

        public string LoadDisplayText
        {
            get => (string)GetValue(LoadDisplayTextProperty);

            set => SetValue(LoadDisplayTextProperty, value);
        }

        public string Row1ToolTipText
        {
            get => (string)GetValue(Row1ToolTipTextProperty);

            set => SetValue(Row1ToolTipTextProperty, value);
        }

        public string Row2ToolTipText
        {
            get => (string)GetValue(Row2ToolTipTextProperty);

            set => SetValue(Row2ToolTipTextProperty, value);
        }
    }
}
