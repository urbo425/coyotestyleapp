﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace CoyoteStyleGuide.Themes
{
    public sealed class DualActionButton : Control
    {
        public static DependencyProperty Button1TextProperty = DependencyProperty.Register(nameof(Button1Text), typeof(string), typeof(DualActionButton), new PropertyMetadata(null));
        public static DependencyProperty Button2TextProperty = DependencyProperty.Register(nameof(Button2Text), typeof(string), typeof(DualActionButton), new PropertyMetadata(null));
        public DualActionButton()
        {
            this.DefaultStyleKey = typeof(DualActionButton);
            //this.dab;
            //this.FindName
            //this.name
            //this.Style.Setters[0].va
        }

        public string Button1Text
        {
            get => (string)GetValue(Button1TextProperty);

            set => SetValue(Button1TextProperty, value);
        }

        public string Button2Text
        {
            get => (string)GetValue(Button2TextProperty);

            set => SetValue(Button2TextProperty, value);
        }
    }
}
